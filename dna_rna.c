/*
 * Program to transcribe a
 * DNA strand to an RNA strand.
 *
 * @Trent Clostio | tclostio.github.io
 */
#include <stdio.h>
#include <string.h>

/*
   transcribe dna to rna
*/

int main()
{
    char DNA[100];
    char RNA[100];

    memset(RNA, '\0', sizeof(RNA));
    strcpy(DNA, "ACGGGAGGACGGGAAAATTACTACGGATTAGC");
    strcpy(RNA, DNA);

    int i;

    for (i = 0; DNA[i] != '\0'; i++) {
        if (DNA[i] == 'T') RNA[i] = 'U'
            ;
    }
    printf("\nHere's the DNA: %s\n", DNA);
    printf("Here's the RNA: %s\n\n", RNA);

    return 0;
}
