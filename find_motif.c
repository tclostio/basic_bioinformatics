/*
 * This program opens a .pep file containing
 * protein sequence data, then manipulates
 * the data.
 *
 * @Trent Clostio | tclostio.github.io
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char *read_from_file(const char *filename)
{
    long int size = 0;
    FILE *file = fopen(filename, "r");

    if (!file) {
        fputs("File Error.\n", stderr);
        return NULL;
    }
    
    fseek(file, 0, SEEK_END);
    size = ftell(file);
    rewind(file);

    char *result = (char *) malloc(size);
    if (!result) {
        fputs("Memory error.\n", stderr);
        return NULL;
    }

    if (fread(result, 1, size, file) != size) {
        fputs("Read error.\n", stderr);
        return NULL;
    }

    fclose(file);
    return result;
}

int main(int argc, char **argv)
{
    if(argc < 2) {
        printf("USAGE: ./read_protein [filename].pep\n\n");
        return -1;
    }

    char *result = read_from_file(argv[1]);
    char *motif = NULL;
    int i;

    if (!result) return -1;

    printf("\nEnter motif to search:");
    scanf(motif);
    
    for (i = 0; i < strlen(result); i++) {
        if (result[i] == motif) {
            printf("\nMatch found!\n");
        } else {
            printf("\nNo matches.\n");
        }
    }
    free(result);

    return 0;
}
