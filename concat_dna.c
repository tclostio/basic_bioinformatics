/*
 * This program stores two variables (
 * DNA strings) and combines them.
 *
 * @Trent Clostio | tclostio.github.io
*/
#include <stdio.h>

int main()
{
    static const char DNA_1[] = "ACGGGAGGACGGGAAAATTACTACGGATTAGC";
    static const char DNA_2[] = "ATAGTGCCGTGAGAGTGATGTAGTA";
    printf("Here are the original two DNA fragments:\n\n");
    printf("%s\n\n", DNA_1);
    printf("%s\n\n", DNA_2);

    printf("And here they are concatenated:\n\n");
    printf("%s%s\n\n", DNA_1, DNA_2);
    
    return 0;
}
