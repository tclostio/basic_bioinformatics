/*
 * Do a simple reverse on a strand of DNA.
 *
 * @Trent Clostio | tclostio.github.io
 */
#include <string.h>
#include <stdio.h>

/* 
 * Function to reverse a string in place.
 * Courtesy of K&R C.
 */
void reverse(char s[])
{
    int length = strlen(s);
    int c, i, j;

    for (i = 0, j = length - 1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* Main method, takes the DNA string. */
int main()
{
    char dna[] = "ACGGGAGGACGGGAAAATTACTACGGCATTAGC";

    printf("\nOriginal Strand: %s\n\n", dna);
    reverse(dna);
    printf("Reversed Strand: %s\n\n", dna);

    return 0;
}
