/*
 * Compute the reverse compliment
 * of a strand of DNA.
 *
 * @Trent Clostio | tclostio.github.io
 */
#include <string.h>
#include <stdio.h>

/* 
 * Function to compute the reverse
 * complement of a given strand.
 */
void reverse(char s[])
{
    int length = strlen(s);
    int i;

    for (i = 0; i < length - 1; i++) {
        if (s[i] == 'A') {
            s[i] = 'T';
        } else if (s[i] == 'T') {
            s[i] = 'A';
        } else if (s[i] == 'G') {
            s[i] = 'C';
        } else if (s[i] == 'C') {
            s[i] = 'G';
        } else {
            s[i] = i;
        }
    }
}

/* Main method, takes the DNA string. */
int main()
{
    char dna[] = "ACGGGAGGACGGGAAAATTACTACGGCATTAGC";

    printf("\nOriginal Strand: %s\n\n", dna);
    reverse(dna);
    printf("Reversed Strand: %s\n\n", dna);

    return 0;
}
